const { ResponseOK, ResponseError, BadRequestException } = require('will-core-lib/http')
const { JoiValidation } = require('will-core-lib/validators')
const AuthenticationService = require('./../Services/AuthenticateService')
const { refresh: refreshSchema } = require('./../SchemaValidator/AuthValidator')

module.exports.handler = async ({ body, headers }) => {
    try {
        const xApiKey = headers['x-api-key']
        body = JSON.parse(body)

        await JoiValidation(refreshSchema, body)
            
        return new ResponseOK({ 
            token: await AuthenticationService.refreshToken(body.token, xApiKey) 
        })
    } catch (error) {
        return new ResponseError(error)
    }
}
