const { ResponseSuccess, ResponseError, UnauthorizedException, BadRequestException } = require('will-core-lib/http')
const { JoiValidation } = require('will-core-lib/validators')
const UsersRepository = require('./../Repositories/UsersRepository')
const CryptService = require('./../Services/CryptService')
const AuthenticationService = require('./../Services/AuthenticateService')
const { access: accessSchema } = require('./../SchemaValidator/AuthValidator')
const AccessHistoryRepository = require('../Repositories/AccessHistoryRepository')

module.exports.handler = async ({ body, headers }) => {
    try {
        const platformKey = headers['x-api-key']
        body = JSON.parse(body)

        await JoiValidation(accessSchema, body)

        const UsersModel = await UsersRepository.getModel()
        const user = await UsersModel.findOne(
        {
            login: body.login,
            platformKey
        })
        .select('+password')

        if (!user || user.deletedAt) throw new UnauthorizedException('Credential Invalid')
        if (!await CryptService.compare(user.password, body.password)) throw new UnauthorizedException('Credential Invalid')
        
        const token = await AuthenticationService.createToken({ _id: user._id, custom: body.custom }, platformKey)

        user.lastLogin = new Date()

        await UsersRepository.save(user)

        await AccessHistoryRepository.create({
            userId: user._id
        })

        return new ResponseSuccess({ token })
    } catch (error) {
        return new ResponseError(error)
    }
}
