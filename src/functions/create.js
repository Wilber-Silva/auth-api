const { ResponseCreated, ResponseImUsed, ResponseError, UnauthorizedException } = require('will-core-lib/http')
const { JoiValidation } = require('will-core-lib/validators')
const UsersRepository = require('./../Repositories/UsersRepository')
const AccessHistoryRepository = require('./../Repositories/AccessHistoryRepository')
const CryptService = require('./../Services/CryptService')
const AuthenticationService = require('./../Services/AuthenticateService')
const { create: createSchema } = require('./../SchemaValidator/AuthValidator')

module.exports.handler = async ({ body, headers }) => {
    try {
        const platformKey = headers['x-api-key']
        body = JSON.parse(body)

        await JoiValidation(createSchema, body)

        const founded = await UsersRepository.findOne({
            login: body.login
            , platformKey
        })
        if (founded) {
            if (founded.deletedAt) return new ResponseImUsed('User has been deleted')

            throw new UnauthorizedException('User Already Exists')
        }

        body.password = await CryptService.generateHash(body.password)
        body.platformKey = platformKey

        const { _id } = await UsersRepository.create(body)

        await AccessHistoryRepository.create({
            userId: _id
        })
        
        const token = await AuthenticationService.createToken({ _id, custom: body.custom }, platformKey)
        
        return new ResponseCreated({ _id, token })
    } catch (error) {
        return new ResponseError(error)
    }
}
