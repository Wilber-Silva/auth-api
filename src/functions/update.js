const { ResponseNotContent, ResponseError, NotFoundException } = require('will-core-lib/http')
const { JoiValidation } = require('will-core-lib/validators')
const UsersRepository = require('./../Repositories/UsersRepository')
const CryptService = require('./../Services/CryptService')
const { update: updateSchema } = require('./../SchemaValidator/AuthValidator')

module.exports.handler = async ({ body, headers, pathParameters: { id } }) => {
    try {
        const platformKey = headers['x-api-key']
        body = JSON.parse(body)
        id = await UsersRepository.convertToObjectId(id)

        await JoiValidation(updateSchema, body)
    
        if (body.password)
            body.password = await CryptService.generateHash(body.password)
       
        const { nMatched, nModified } = await UsersRepository.update(
            { _id: id, platformKey }
            , body
        )

        if ( nMatched > 0 || nModified > 0 ) {
            return new ResponseNotContent()
        }

        throw new NotFoundException('User not found')
    } catch (error) {
        return new ResponseError(error)
    }
}
