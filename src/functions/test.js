const { ResponseSuccess } = require('will-core-lib/http')

module.exports.handler = async () => new ResponseSuccess({ test: 'ok' })
