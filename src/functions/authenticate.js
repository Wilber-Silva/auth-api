const { ResponseSuccess, ResponseError } = require('will-core-lib/http')
const { JoiValidation } = require('will-core-lib/validators')
const AuthenticationService = require('./../Services/AuthenticateService')
const { refresh: refreshSchema } = require('./../SchemaValidator/AuthValidator')

module.exports.handler = async ({ body, headers }) => {
    try {
        const xApiKey = headers['x-api-key']
        const { token } = JSON.parse(body)
        
        await JoiValidation(refreshSchema, { token })
        
        await AuthenticationService.authenticate(token, xApiKey)
        const object = await AuthenticationService.decode(token)

        return new ResponseSuccess(object)
    } catch (error) {
        console.log(error)
        return new ResponseError(error)
    }
}