const Joi = require('@hapi/joi')
Joi.objectId = require('joi-objectid')(Joi)

const login = Joi.string().max(256)
const password = Joi.string().min(6)
const custom = Joi.object()

module.exports.create = Joi.object({
    login: login.required(),
    password: password.required(),
    custom
})

module.exports.update = Joi.object({
    login,
    password
})

module.exports.access = Joi.object({
    login: login.required(),
    password: password.required(),
    custom
})

module.exports.refresh = Joi.object({
    token: Joi.string().required()
})

module.exports.delete = Joi.object({
    id: Joi.objectId().required()
})