
const mongoose = require('mongoose')

module.exports.AccessHistorySchema = new mongoose.Schema({
    userId: { type: mongoose.Types.ObjectId }
    , createdAt: { type: Date, default: Date.now }
    , updatedAt: { type: Date, default: null }
    , deletedAt: { type: Date, default: null }
})
