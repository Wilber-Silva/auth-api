
const mongoose = require('mongoose')

module.exports.UsersSchema = new mongoose.Schema({
    login: { type: String, require: true},
    password: { type: String, require: true, select: false },
    platformKey: { type: String },
    lastLogin: { type: Date, default: Date.now },
    createdAt: { type: Date, default: Date.now },
    updatedAt: { type: Date, default: null },
    deletedAt: { type: Date, default: null }
})
