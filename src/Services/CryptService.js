class CryptService {
    constructor () {
        this.crypt = require('bcryptjs')
        this.salt = 0
    }
    generateSalt = async (saltRounds = 10) => {
        this.salt = await this.crypt.genSaltSync(saltRounds)
    }
    generateHash = async (crypt = '') => {
        if (!this.salt) await this.generateSalt()
        const hash = await this.crypt.hashSync(crypt, this.salt)
        return hash 
    }
    compare = async (hash, valueCompare) => {
        const result = await this.crypt.compareSync(valueCompare, hash) 
        return result
    }
}

module.exports = new CryptService()