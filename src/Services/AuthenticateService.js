const jwt = require('jsonwebtoken')
const { UnauthorizedException } = require('will-core-lib/http')

class AuthenticateService {
    createToken = async (object, key) => {
        object.createdAt = new Date()
        return jwt.sign(object, key, { expiresIn: 60 * process.env.ACCESS_TIMEOUT })
    }
    decode = async (token) => {
        return jwt.decode(token)
    }
    refreshToken = async (token, key) => {
        const object = await this.decode(token)
        const { _id, custom } = object
        token = await this.createToken({ _id, custom }, key)
        return token
    }
    authenticate = async (token, key) => {   
        try {
            const object = jwt.verify(token, key)
            if ( !object._id ) throw new UnauthorizedException('Token malformed')
            return 
        } catch (error) {
            throw new UnauthorizedException('Token expired')
        }
    }
}

module.exports = new AuthenticateService()