const { BaseRepository, MongoConnection } = require("will-core-lib/connectors/mongodb")
const { UsersSchema } = require('./../Models/users')


class Repository extends BaseRepository {
    constructor() {
        super('users', UsersSchema, new MongoConnection(process.env.MONGODB))
    }
}

module.exports = new Repository()