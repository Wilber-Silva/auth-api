const { BaseRepository, MongoConnection } = require("will-core-lib/connectors/mongodb")
const { AccessHistorySchema } = require('./../Models/access-history')


class Repository extends BaseRepository {
    constructor() {
        super('access_history', AccessHistorySchema, new MongoConnection(process.env.MONGODB))
    }
}

module.exports = new Repository()