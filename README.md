# README #

Created using NodeJs v12

* Access control of users

## Features ##

Features Documentations

### Create User Function ###

* Method: POST
* Uri: /

* Request example

```json
{
    "login": "example@example.com", // required
    "password": "123456", // required
    "custom": {} // optional custom object to aggregate into token
}
```

* Response 200

```json
{
    "_id": "5ee3940610513b1220ac38b9",
    "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI1ZWUzOTQwNjEwNTEzYjEyMjBhYzM4YjkiLCJpYXQiOjE1OTE5NzI4NzAsImV4cCI6MTU5MTk3MzE3MH0.Vb7UM-75uuzzs5tsvFwe26RrB7xrCajlmBRKqENvUNw"
}
```

* Response 400 Bad request

```json
// Request
{
    "login": "example.com",
    "password": "123456"
}
// Response
{
    "message": "\"login\" must be a valid email.",
    "errors": [ {
        "message": "\"login\" must be a valid email",
        "path": [
            "login"
        ],
        "type": "string.email",
        "context": {
            "value": "example.com",
            "invalids": [
                "example.com"
            ],
            "label": "email",
            "key": "email"
        }
    } ]
}
```

* Response 422 Unprocessable Entity

```json
{
    "message": "User Already Exists"
}
```

### Grant User Access Function ###

* METHOD: POST
* Uri: /access

* Request example

```json
{
    "login": "example@example.com", // required
    "password": "123456", // required
    "custom": {} // optional custom object to aggregate into token
}
```

* Response 200

```json
{
    "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI1ZWUzOWJiNzQwMTA4NDBmNjBhMmQwYjkiLCJpYXQiOjE1OTE5NzQ4NDYsImV4cCI6MTU5MTk3NTE0Nn0.cnI-AqZ-L_2jYwFX4e5b15a8nmA47rkl-GNZCLadeE0"
}
```

* Response 400 Bad Request

```json
// Request
{
    "login": "example.com",
    "password": "123456"
}
// Response
{
    "message": "\"login\" must be a valid email.",
    "errors": [ {
        "message": "\"login\" must be a valid email",
        "path": [
            "login"
        ],
        "type": "string.email",
        "context": {
            "value": "example.com",
            "invalids": [
                "example.com"
            ],
            "label": "email",
            "key": "email"
        }
    } ]
}
```

* Response 401 Unauthorized

```json
{
    "message": "Credential Invalid"
}
```

### Refresh Access Function ###

* METHOD: POST
* Uri: /refresh

* Request example

```json
{
    "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI1ZWUzOWJiNzQwMTA4NDBmNjBhMmQwYjkiLCJpYXQiOjE1OTE5NzYzOTEsImV4cCI6MTU5MTk3NjY5MX0.82ZBLBU0EQoy-DuONVmDeGCtfOnSfEaQpjyNFisvLKs" // Required
}
```

* Response 200

```json
{
    "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI1ZWUzOWJiNzQwMTA4NDBmNjBhMmQwYjkiLCJpYXQiOjE1OTE5NzQ4NDYsImV4cCI6MTU5MTk3NTE0Nn0.cnI-AqZ-L_2jYwFX4e5b15a8nmA47rkl-GNZCLadeE0"
}
```

* Response 400

```json
{
    "message": "\"token\" is required",
    "errors": [
        {
            "message": "\"token\" is required",
            "path": [
                "token"
            ],
            "type": "any.required",
            "context": {
                "label": "token",
                "key": "token"
            }
        }
    ]
}
```

### Authenticate Access Function ###

* METHOD: POST
* Uri: /authenticate

* Request example

```json
{
    "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI1ZWUzOWJiNzQwMTA4NDBmNjBhMmQwYjkiLCJpYXQiOjE1OTE5NzYzOTEsImV4cCI6MTU5MTk3NjY5MX0.82ZBLBU0EQoy-DuONVmDeGCtfOnSfEaQpjyNFisvLKs" // Required
}

* Response 400

```json
{
    "message": "\"token\" is required",
    "errors": [
        {
            "message": "\"token\" is required",
            "path": [
                "token"
            ],
            "type": "any.required",
            "context": {
                "label": "token",
                "key": "token"
            }
        }
    ]
}
```

* Response 401 Unauthorized

```json
{
    "message": "Token expired"
}
```
